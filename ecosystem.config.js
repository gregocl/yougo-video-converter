module.exports = [{
    script: 'dist/index.js',
    exp_backoff_restart_delay: 100,
    name: 'videos-processor',
    env: {
      NODE_ENV: "production",
    },
  }]
  