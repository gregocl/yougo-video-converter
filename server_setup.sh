sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install unzip
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
apt-get install -y nodejs
sudo npm i -g typescript nodemon yarn pm2
sudo apt-get install ffmpeg
sudo apt-get install git
sudo apt-get update

