import { processMsg } from './controller';
import {
    logger,
    validateConfig,
    setupDirectories,
    connectRabbitWithRetries,
    printStartupLog,
} from './utils';

const QUEUE_NAME = 'VIDEOS_TO_PROCESS';

(async () => {
    printStartupLog();
    await validateConfig();
    await setupDirectories();

    const rabbitConnection = await connectRabbitWithRetries();
    const closeAndExitIfErr = async (err?: Error | true) => {
        if (!err) return;
        console.error("[AMQP] error", err);
        await rabbitConnection.close();
        process.exit();
    }

    try {
        const channel = await rabbitConnection.createChannel();
        channel.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
            closeAndExitIfErr(err);
        });
        channel.on("close", function () {
            logger.info("[AMQP] channel closed");
            closeAndExitIfErr(true);
        });
        channel.prefetch(1);
        await channel.assertQueue(QUEUE_NAME, { durable: true });
        channel.consume(QUEUE_NAME, (msg) => processMsg(msg, channel), { noAck: false });
        logger.info("Consumer is started");
    } catch (error) {
        logger.error("[AMQP error", error.message);
        closeAndExitIfErr(true);
    }
})();
