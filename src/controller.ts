import { Channel, ConsumeMessage } from 'amqplib';
import axios from 'axios';

import {
    config,
    mergeAndConvertToMp4,
    deleteSourceFiles,
    deleteTempFile,
    logger,
    uploadFileToS3,
    getSourceFilesFromS3,
    VideoAttributes,
    throwIfFilesDontExist,
    printVideoLogsHeader,
    deleteAllFilesFromFolder
} from './utils';

export const processMsg = async (msg: ConsumeMessage | null, channel: Channel) => {
    const videoID = msg?.content?.toString();
    if (!msg || !videoID) {
        !msg && console.error('no message received !!!');
        throw new Error('message has no videoID');
    }
    printVideoLogsHeader(videoID);

    try {
        const videoData = await fetchVideoData(videoID);
        const { file1, file2, file3, file4, uploaded, processed, processed_file, sound_file } = videoData;

        const videoNames = [file1, file2, file3, file4].filter(Boolean) as string[];
        // Abort if video should not be processed
        if (processed || !uploaded || !videoNames.length) {
            logger.info(`aborting, video should not be processed: 
                uploaded: ${uploaded}
                processed: ${processed}
                videos: ${videoNames.join(', ')}
            `);
            channel.ack(msg);
            return;
        }
        // Get source files
        const files = [...videoNames, sound_file].filter(Boolean) as string[];
        logger.info(`start processing files ${files.join(', ')}`);
        await throwIfFilesDontExist(files);
        const videoFiles = await getSourceFilesFromS3(videoNames);
        const audioFile = sound_file && (await getSourceFilesFromS3([sound_file]))?.[0];
        // Process files
        logger.info('start converting & merging files');
        const filePath = await mergeAndConvertToMp4(videoFiles, audioFile, processed_file);
        logger.info(`conversion succeeded, result file: ${filePath}`);
        // Upload & delete resulting file
        await uploadFileToS3(filePath);
        logger.info(`upload to S3 succeeded for file: ${filePath}`);
        await deleteTempFile(filePath);
        logger.info(`deletion succeeded for temp file: ${filePath}`);
        await deleteAllFilesFromFolder(config.SOURCE_FILES_DIR);
        logger.info(`removed downloaded files`);
        // Remove source files from S3
        if (config.DELETE_FROM_S3 && +config.DELETE_FROM_S3) {
            const filesToDelete = [...videoNames, sound_file].filter(Boolean) as string[];
            await deleteSourceFiles(filesToDelete);
            logger.info(`deletion succeeded for S3 source files: ${[...videoNames, sound_file].filter(Boolean)}`);
        } else logger.info('skiping delete source files from S3');
        logger.info(`file conversion successfully finished`);
        // Notify backend
        await axios.get(`${config.SERVER_URL}/processed/${videoID}`, { headers: { authorization: config.API_KEY } });
        logger.info('backend successfully notified');
        channel.ack(msg);
        logger.info(`Video ${videoID} sucessfully processed`);
    } catch (error) {
        logger.error(`error processing message`);
        logger.error(error);
        channel.reject(msg, false);
    }
}


const fetchVideoData = async (videoId: string) => {
    const { data: { video: videoData } } = await axios.get<{ video: VideoAttributes }>(
        `${config.SERVER_URL}/video/${videoId}`,
        {
            headers: { authorization: 'api_key' }
        });
    return videoData;
}