import { createLogger, format, transports } from 'winston';
 
const { combine, timestamp, label, printf, colorize, json } = format;

const logFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

export const cameraLogger = createLogger({
  level: process.env.LOGGER_LEVEL,
  format: combine(
    colorize(),
    label({ label: 'camera' }),
    timestamp(),
    logFormat
  ),
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    // new winston.transports.File({ filename: 'error.log', level: 'error' }),
    // new winston.transports.File({ filename: 'combined.log' }),
    new transports.Console({ level: 'debug' }),
  ],
});
 
const logger = createLogger({
  level: process.env.LOGGER_LEVEL,
  format: combine(
    colorize(),
    label({ label: 'logger' }),
    timestamp(),
    logFormat
  ),
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    // new winston.transports.File({ filename: 'error.log', level: 'error' }),
    // new winston.transports.File({ filename: 'combined.log' }),
    new transports.Console({ level: 'debug' }),
  ],
});
 
export const printStartupLog = () => {
  const now = new Date();
  console.log(`
  
  
  
*****************************************************
* 
*       Videos processor program starting
*       ${now.toLocaleString()}
* 
*****************************************************
  `);
}
 
export const printVideoLogsHeader = (videoId: string) => {
  logger.info('\n');
  logger.info(`***********************************************************************`);
  logger.info(`*                                                                     *`);
  logger.info(`*   Processing video ${videoId}                                       *`);
  logger.info(`*                                                                     *`);
  logger.info(`***********************************************************************`);
}


//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
// if (process.env.NODE_ENV !== 'production') {
//   logger.add(new winston.transports.Console({
//     format: winston.format.simple(),
//   }));

export default logger;