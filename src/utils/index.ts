export * from './buckets';
export * from './config';
export * from './ffmpeg';
export * from './files';
export * from './streams';
export * from './logger';
export * from './rabbit';
export { default as logger } from './logger';

export interface VideoAttributes {
  id: string;
  user_email: string;
  file1: string | null;
  file2: string | null;
  file3: string | null;
  file4: string | null;
  sound_file: string | null;
  processed_file: string;
  uploaded: boolean;
  processed: boolean;
  deleted: boolean;
  creation_date: Date;
  deletion_date: Date | null;
};
