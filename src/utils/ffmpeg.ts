import fs from 'fs';
import ffmpeg from 'fluent-ffmpeg';
import { Readable } from 'stream';
import path from 'path';

import { logger, deleteAllFilesFromFolder } from '.';
import { config } from './config';


type TMergeAndConvertInput = {
  stack: 'v' | 'h';
  video1: string;
  video2: string;
  outputFilePath: string
}

const mergeAndConvert2Videos = ({ stack, video1, video2, outputFilePath }: TMergeAndConvertInput): Promise<string> => new Promise((resolve, reject) => {
  ffmpeg()
    .videoCodec('libx264')
    .audioCodec('aac')
    .addInput(video1)
    .addInput(video2)
    .output(outputFilePath)
    .complexFilter([`${stack}stack`])
    .on('error', (error: Error) => {
      logger.error(`error in ffmpeg: ${error.message}`);
      reject(error);
    })
    .on('end', () => {
      logger.info(`ffmpeg ended: mergeAndConvert2Videos successful`);
      resolve(outputFilePath);
    })
    .run();
})


const addVerticalPadding = (videoFilePath: string, outputFilePath: string, audioFile?: string | null): Promise<string> => new Promise((resolve, reject) => {
  const command = ffmpeg()
    .videoCodec('libx264')
    .audioCodec('aac')
    .addInput(videoFilePath)
    .output(outputFilePath)
    .videoFilter([`pad=width=iw*2:height=ih:x=iw/2:y=0:color=black`])
    .on('error', (error: Error) => {
      logger.error(`error in ffmpeg: ${error.message}`);
      reject(error);
    })
    .on('end', () => {
      logger.info(`fmpeg ended: addVerticalPadding successful`);
      resolve(outputFilePath);
    })
  // if (stack === 'h') command.complexFilter([`[0]pad=iw+5:color=black[left]`, `[left][1]hstack=inputs=2`])
  // else command.complexFilter([addPadding ? '[0:v]pad=iw*2:ih[int];[int][1:v]overlay=W/2:0[vid]' : `vstack`]);
  // addPadding && command.map('[vid]').preset('veryfast');

  audioFile && command.addInput(audioFile);
  command.run();
});

const convertH264ToMp4NoSound = (videoFile: string | Readable, outputFilePath: string): Promise<void> => new Promise((resolve, reject) => {
  ffmpeg()
    .input(videoFile)
    .videoCodec('libx264')
    .on('error', (error: Error) => {
      logger.error(`error in ffmpeg: ${error.message}`);
      reject(error);
    })
    .on('end', () => {
      logger.info(`ffmpeg ended: convertH264ToMp4NoSound successful`);
      resolve();
    })
    .saveToFile(outputFilePath);
})

const convertH264ToMp4 = (videoFile: string | Readable, soundFilePath: string, outputFilePath: string): Promise<void> => new Promise((resolve, reject) => {
  ffmpeg()
    .input(videoFile)
    .videoCodec('libx264')
    .input(soundFilePath)
    .audioCodec('aac')
    .on('error', (error: Error) => {
      logger.error(`error in ffmpeg: ${error.message}`);
      reject(error);
    })
    .on('end', () => {
      logger.info(`ffmpeg ended: convertH264ToMp4 successful`);
      resolve();
    })
    .saveToFile(outputFilePath);
})

export const mergeAndConvertToMp4 = (videoNames: string[], audioFileName: string | null, targetFileName?: string): Promise<string> =>
  new Promise(async (resolve, reject) => {
    try {
      // const readStreams = videoNames.map((name) => setupStreamEvents(getReadStreamFromS3(name), name));
      const audioFilePath = audioFileName && path.resolve(process.cwd(), config.SOURCE_FILES_DIR, audioFileName);
      const outputVideoName = targetFileName || videoNames[0].split('/')[videoNames[0].split('/').length - 1].replace('h264', 'mp4');
      const outputFilePath = path.resolve(process.cwd(), `${config.PROCESSED_FILES_DIR}/${outputVideoName}`);
      const tempVideosDirPath = path.resolve(process.cwd(), config.TEMP_FILES_DIR);
      const nbOfVideoFiles = videoNames.length as 1 | 2 | 3 | 4;
      const soundCamId = audioFileName?.split('_')[0];
      const videoToMergeSoundWith = soundCamId && videoNames.find(fileName => fileName.includes(`_${soundCamId}`));
      // If only 1 video to process
      if (nbOfVideoFiles === 1) {
        audioFilePath ?
          await convertH264ToMp4(fs.createReadStream(videoNames[0]), audioFilePath, outputFilePath)
          :
          await convertH264ToMp4NoSound(fs.createReadStream(videoNames[0]), outputFilePath);

        await deleteAllFilesFromFolder(config.TEMP_FILES_DIR);
        resolve(outputFilePath);
        return;
      }

      let videoFilesToMerge = [...videoNames];
      
      const videoWithSoundPath = `${tempVideosDirPath}/sound_${outputVideoName}`;
      if (videoToMergeSoundWith) {
        await convertH264ToMp4(videoToMergeSoundWith, audioFilePath as string, videoWithSoundPath);
        videoFilesToMerge = [videoWithSoundPath, ...videoFilesToMerge.filter(file => file !== videoToMergeSoundWith)];
      } else if (audioFilePath){
        await convertH264ToMp4(videoNames[0], audioFilePath as string, videoWithSoundPath);
        videoFilesToMerge = [videoWithSoundPath, ...videoFilesToMerge.filter(file => file !== videoNames[0])];
      }
      
      // If 2 or more videos to process, merge the 2 first ones horizontally
      const firstMergedVideoPath = `${tempVideosDirPath}/1_${outputVideoName}`;
      await mergeAndConvert2Videos({
        stack: 'h',
        video1: videoFilesToMerge[0],
        video2: videoFilesToMerge[1],
        outputFilePath: nbOfVideoFiles > 2 ? firstMergedVideoPath : outputFilePath,
      });

      logger.info('successfully processed first horizontal merge');
      // Return if only one 2 source videos
      if (nbOfVideoFiles === 2) {
        await deleteAllFilesFromFolder(config.TEMP_FILES_DIR);
        resolve(outputFilePath);
        return;
      }

      const secondMergedVideoPath = `${tempVideosDirPath}/2_${outputVideoName}`;

      if (nbOfVideoFiles === 3) {
        await addVerticalPadding(videoFilesToMerge[2], secondMergedVideoPath);
        logger.info('successfully added horizontal padding to 3rd video');
      } else {
        // Process 2nd merge
        await mergeAndConvert2Videos({
          stack: 'h',
          video1: videoFilesToMerge[2],
          video2: videoFilesToMerge[3],
          outputFilePath: secondMergedVideoPath
        });
        logger.info('successfully processed second horizontal merge');
      }

      await mergeAndConvert2Videos({
        stack: 'v',
        video1: firstMergedVideoPath,
        video2: secondMergedVideoPath,
        outputFilePath
      });
      logger.info('successfully processed vertical merge');
      // Delete 3rd merge source files
      await deleteAllFilesFromFolder(config.TEMP_FILES_DIR);

      logger.info('successfully processed videos');
      resolve(outputFilePath);
    } catch (error) {
      logger.error(`Error in processVideo: `, error);
      logger.error(error);
      reject();
    }
  });