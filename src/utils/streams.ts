import { Readable, Writable } from 'stream';

import { logger } from '.';

type StreamType = Readable | Writable;

export function setupStreamEvents<T extends StreamType>(stream: T, streamName: string): T {
  stream.on('error', (error: Error) => {
    logger.error(`${streamName || 'stream'} error: ${error.message}`, error);
  })
  stream.on('end', () => {
    logger.info(`${streamName || 'stream'}: stream ended`);
  })
  // stream.on('finish', () => {
  //   logger.info(`${streamName || 'stream'}: stream finished`);
  // })
  stream.on('close', () => {
    logger.info(`${streamName || 'stream'}: stream closed`);
  });
  return stream;
};
