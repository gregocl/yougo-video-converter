import * as yup from 'yup';
import dotenv from 'dotenv';

import { logger } from '.';
dotenv.config();

export interface ConfigType {
  LOGGER_LEVEL: string;
  API_KEY: string;
  S3_RAW_VIDEOS_BUCKET: string;
  S3_PROCESSED_VIDEOS_BUCKET: string;
  DELETE_FROM_S3?: string;
  SOURCE_FILES_DIR: string;
  TEMP_FILES_DIR: string;
  PROCESSED_FILES_DIR: string;
  CLOUDAMQP_URL: string;
  CLOUDAMQP_APIKEY: string;
  SERVER_URL: string;
};

export const config = {
  LOGGER_LEVEL: process.env.LOGGER_LEVEL,
  API_KEY: process.env.API_KEY,
  S3_RAW_VIDEOS_BUCKET: process.env.S3_RAW_VIDEOS_BUCKET,
  S3_PROCESSED_VIDEOS_BUCKET: process.env.S3_PROCESSED_VIDEOS_BUCKET,
  DELETE_FROM_S3: process.env.DELETE_FROM_S3,
  SOURCE_FILES_DIR: 'source_files',
  TEMP_FILES_DIR: 'temp_files',
  PROCESSED_FILES_DIR: 'processed_files',
  CLOUDAMQP_URL: process.env.CLOUDAMQP_URL,
  CLOUDAMQP_APIKEY: process.env.CLOUDAMQP_APIKEY,
  SERVER_URL: process.env.SERVER_URL,
} as ConfigType;


const keysSchema = yup.object().shape({
  LOGGER_LEVEL: yup.string().required(),
  API_KEY: yup.string().required(),
  S3_RAW_VIDEOS_BUCKET: yup.string().required(),
  S3_PROCESSED_VIDEOS_BUCKET: yup.string().required(),
  DELETE_FROM_S3: yup.number().notRequired(),
  SOURCE_FILES_DIR: yup.string().required(),
  TEMP_FILES_DIR: yup.string().required(),
  PROCESSED_FILES_DIR: yup.string().required(),
  CLOUDAMQP_URL: yup.string().required(),
  CLOUDAMQP_APIKEY: yup.string().required(),
  SERVER_URL: yup.string().required(),
})

export const validateConfig = async () => {
  try {
    await keysSchema.validate(config);
    logger.log('info', 'env configuration validated');
  } catch (err) {
    logger.error('Error validating the .env file');
    logger.error(err.errors); // => [{ key: 'field_too_short', values: { min: 18 } }]
    logger.log('info', 'EXITING !!');
    process.exit();
  }
}
