import { Readable, pipeline } from 'stream';

import fs from 'fs';
import path from 'path';
import AWS from 'aws-sdk';

import { config, logger } from '.';
import { setupStreamEvents } from './streams';

const s3 = new AWS.S3({
  apiVersion: 'latest',
});

export const uploadFileToS3 = (filePath: string) => new Promise((resolve, reject) => {
  try {
    if (!config.S3_PROCESSED_VIDEOS_BUCKET) {
      throw new Error('no target bucket specified ! Aborting upload');
    }
    const fileName = filePath.split('/')[filePath.split('/').length - 1];
    const completePath = path.resolve(process.cwd(), filePath);
    const readStream = fs.createReadStream(completePath);
    logger.info(`start uploading to S3, file: ${fileName}`);
    // Setting up S3 upload parameters
    const params = {
      Bucket: config.S3_PROCESSED_VIDEOS_BUCKET,
      Key: fileName, // File name you want to save as in S3
      Body: readStream
    };

    // Uploading files to the bucket
    s3.upload(params, function (err: Error | undefined, data: any) {
      if (err) {
        logger.error(`error in s3.upload: ${err.message}`);
        logger.error(err);
        reject(err);
      }
      resolve(data);
    });
  } catch (error) {
    logger.error(`error in uploadFileToS3: ${error.message}`);
    logger.error(error);
    reject(error);
  }
});

export const getReadStreamFromS3 = (videoFileName: string): Readable => {
  try {
    const params = { Bucket: config.S3_RAW_VIDEOS_BUCKET, Key: videoFileName };
    return s3.getObject(params).createReadStream();
  } catch (error) {
    throw new Error('unable to get file stream from S3');
  }
}

export const deleteSourceFiles = (videoFileNames: string[]): Promise<void> => new Promise((resolve, reject) => {
  const promises: Promise<void>[] = [];
  try {
    for (let fileName of videoFileNames) {
      const params = { Bucket: config.S3_RAW_VIDEOS_BUCKET, Key: fileName };
      promises.push(new Promise((resolve, reject) => {
        logger.info(`start deleting source file: ${fileName}`);
        s3.deleteObject(params, (err) => {
          if (err) {
            logger.error(`error deleting source file: ${err.message}`);
            logger.error(err.stack);
            reject(err);
          }
          resolve();
        });
      }))
    }
    Promise.allSettled(promises)
      .then(() => resolve())
      .catch(err => { throw err });
  } catch (error) {
    logger.error(`Error in deleteSourceFile: ${error.message}`);
    reject(error);
  }
});

export const getSourceFilesFromS3 = (files: string[]): Promise<string[]> => new Promise((resolve, reject) => {
  const promises: Promise<string>[] = [];
  try {
    for (let file of files) {
      promises.push(new Promise((resolve, reject) => {
        logger.info(`start fetching source file: ${file}`);
        const readStream = setupStreamEvents(getReadStreamFromS3(file), `readStream ${file}`);
        const writeStream = setupStreamEvents(fs.createWriteStream(path.resolve(process.cwd(), config.SOURCE_FILES_DIR, file), { flags: 'w' }), `writeStream ${file}`);
        writeStream.on('close', () => resolve(`${config.SOURCE_FILES_DIR}/${file}`))
        pipeline(readStream, writeStream, (err) => {
          if (err) {
            logger.error(`error in pipeline ${file}: ${err.message}`);
            logger.error(err);
            throw err;
          }
        })
      }))
    }
    Promise.all(promises)
      .then(sourceFilesPathes => {
        const completePathes = sourceFilesPathes.map(filePath => path.resolve(process.cwd(), filePath));
        resolve(completePathes)
      })
      .catch(err => { throw err });
  } catch (error) {
    logger.error(`Error in getSourceFilesFromS3: ${error.message}`);
    reject(error);
  }
});

export const throwIfFilesDontExist = async (files: string[]) => {
  try {
    const promises = files.map((fileName) => s3.headObject({ Bucket: config.S3_RAW_VIDEOS_BUCKET, Key: fileName }).promise());
    await Promise.all(promises);
  } catch (error) {
    logger.error(`error verifying source files, job will abort`);
    throw error;
  }
}