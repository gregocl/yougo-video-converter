import * as fs from "fs";
import path from 'path';
import { config, logger } from ".";

export const deleteTempFile = (filePath: string): Promise<void> => new Promise((resolve, reject) => {
  const completePath = path.resolve(process.cwd(), filePath);
  fs.unlink(completePath, (err: any) => {
    if (err) {
      logger.error('error remmoving file');
      reject(err);
      return;
    }
    resolve();
  })
});

export const deleteAllFilesFromFolder = async (directoryName: string): Promise<void> => {
  const filesToDelete = fs.readdirSync(path.resolve(process.cwd(), directoryName));
  await Promise.all(filesToDelete.map(filePath => {
    deleteTempFile(`${directoryName}/${filePath}`)
  }))
};

const setupOutputDir = (outputDirPath: string): Promise<{ error?: any, result: boolean }> => new Promise((resolve) => {
  fs.mkdir(outputDirPath, (error: any) => {
    if (error) {
      // If output directory already exits
      if (error.errno === -17) {
        resolve({ result: true });
        return;
      }
      logger.error('error creating folder : ' + outputDirPath, error);
      resolve({ error, result: false });
      return;
    }
    logger.log('info', 'created folder : ' + outputDirPath);
    resolve({ result: true });
  });
});

export const setupDirectories = async () => {
  await setupOutputDir(config.SOURCE_FILES_DIR);
  await setupOutputDir(config.TEMP_FILES_DIR);
  await setupOutputDir(config.PROCESSED_FILES_DIR);
}