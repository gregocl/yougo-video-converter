import { connect, Connection } from "amqplib";
import { config, logger } from ".";

const MAX_RETRIES = 5;

const connectToRabbit = async (): Promise<Connection> => {
  try {
    const connection = await connect(config.CLOUDAMQP_URL + "?heartbeat=60");
    connection.on("error", (err) => {
      if (err.message !== "Connection closing") {
        logger.error("[AMQP] conn error", err.message);
        throw err;
      }
    });
    connection.on("close", function () {
      logger.error("[AMQP] connection closed");
      return setTimeout(connectToRabbit, 1000);
    });
    logger.info("[AMQP] connected");
    return connection;
  } catch (err) {
    logger.error("[AMQP] error:", err.message);
    throw err;
  }
}


export const connectRabbitWithRetries = async (): Promise<Connection> => {
  let retries = 0;
  do {
    try {
      return await connectToRabbit();
    } catch (error) {
      retries += 1;
    }
  } while (retries <= MAX_RETRIES);
  logger.error(`${MAX_RETRIES} failed RabbitMQ connection attempts, process will exit`);
  process.exit();
}